# 应急管理大学 信息管理实验室 网站材料（初稿 已弃用）

我们是应急管理大学信息管理实验室团队，我们在这里撰写文档，分享资料，推动开源文化与共享精神在中国大地落地生根，开花结果。

这一仓库储存了[我们网站](www.neihewozai.cn)的全部材料。

![1673653990523](image/README/1673653990523.png)

## 如何使用这个仓库？

```shell
# 下载文档仓库
git clone https://gitee.com/IamBeginnerC/NHWZ-Community.git
# 下载网站构建工具
git clone https://gitee.com/IamBeginnerC/all-book-builder.git
cd all-book-builder
make
# 在正式构建前，请先完成 mdbook 的配置
./AllBookBuilder ../NHWZ-Community
```

注意：我们需要的是 [mdbook](https://github.com/earnhardt3rd/rust-mdBook) 的[二进制格式文件](https://sourceforge.net/projects/mdbook.mirror/)

## 我们欢迎你来

与来自各地的人打交道，做朋友是一件非常让人高兴的事情，很高兴能看到你的参与。

1. 提交 Issue
   欢迎提出你的观点，看法与意见。
2. 分享你的项目经验、材料
   优质的内容往往能够吸引优质的人
3. 参加线下交流
   我们会定期组织活动
