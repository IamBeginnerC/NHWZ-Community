# C语言中的逻辑语句与循环语句

循环语句，本质上是一种封装，将同样的代码，根据条件成立与否，决定是否反复执行。

在讲述循环语句之前，我们引入C语言中 if 语句 与 goto 语句的概念。

## if 语句

if 语句的基本形式如下

```c
if (condition)
{
	statement;
}
```

一个具体的案例如下

![1672969012526](image/C语言中的逻辑语句与循环语句/1672969012526.png)

```c
/*
    if_first.c
    Use the if statement to solve the problem
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
int main()
{
    int number = 0;
    printf("Please input a number >= 0\n");
    scanf("%d", &number);
    if (number < 0)
    {
        printf("The number < 0, Quit.\n");
        exit(0);
    }
    printf("You input %d\n", number);
    return 0;
}

```

在这一案例中，我们鼓励用户输入一个 >= 0 的数字，并在用户输入不合法数据的时候，直接选择退出（exit 函数的作用就是直接退出程序，填入的数字0代表我们希望的返回值）

在实际应用中，这相当于为程序上了一道安全阀。

下面我们继续展现 if 语句的应用

![1672970351612](image/C语言中的逻辑语句与循环语句/1672970351612.png)

```c
/*
    if_second.c
    Show the usage of if statement
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    scanf("%d", &number);
    if (number % 2 == 0)
    {
        printf("%d is not a odd number\n", number);
    }
    else
    {
        printf("%d is a odd number\n", number);
    }
    return 0;
}
```

在这一案例之中，我们使用 if-else 语句判断 一个数字是否为奇数

![1672970740905](image/C语言中的逻辑语句与循环语句/1672970740905.png)

```c
/*
    if_third.c
    Use the if statement to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    scanf("%d", &number);
    if (number >= 0)
        if (number > 0)
            printf("%d > 0\n", number);
    else
        printf("%d < 0", number);
    return 0;
}

```

在这一案例中，我们展现了if语句的一种常见错误：if 语句采用就近匹配原则，换而言之，这个程序违背了我们的本意，else 配对的对象是 `if (number > 0)`

而由于这个 if 语句的前置条件是 number >= 0，所以，只有在输入 0 时，这个 else 才会被匹配到

修订为如下，可以顺利地解决这个问题

![1672970989708](image/C语言中的逻辑语句与循环语句/1672970989708.png)

```c
/*
    if_fourth.c
    Use the if statement to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    scanf("%d", &number);
    if (number >= 0)
    {
        if (number > 0)
        {
            printf("%d > 0\n", number);
        }
    }
    else
    {
        printf("%d < 0", number);
    }
    return 0;
}

```

我们当然也可以把等于0的情况进行加入

```c
#include <stdio.h>
int main()
{
    int number = 0;
    scanf("%d", &number);
    if (number >= 0)
    {
        if (number > 0)
        {
            printf("%d > 0\n", number);
        }
    }
    else if (number == 0)
    {
        printf("%d = 0\n", number);
    }
    else
    {
        printf("%d < 0", number);
    }
    return 0;
}

```

## goto 语句

goto 语句是一种无条件的跳转指令，用于快速改变程序的执行流程。

学习一种程序知识的最好办法，就是动手制作程序，在此，我们写作一个简单的程序

```c
/*
    goto.c
    Use the goto to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    goto FLAG;
    printf("Line: %d\n", __LINE__);
    printf("Line: %d\n", __LINE__);
    FLAG:;
    printf("Line: %d\n", __LINE__);
    return 0;
}

```

![1672739188776](image/C语言中的循环语句/1672739188776.png)

这个程序很好地演示了 goto 语句的功能，可以很明确地发现，程序直接跳转到了第十三行（也就是FLAG标签后面的那一行代码），而goto 语句和标签之间的代码，则直接不予执行。

值得注意的是，我们使用了 `__LINE__` 宏，它是一个整数，代表当前的行号。

goto 语句也可以从后向前跳转，如您所见。

![1672739436775](image/C语言中的循环语句/1672739436775.png)

```c
/*
    goto_second.c
    Use the goto to solve the problem
    (Second Version)
    BeginnerC
*/
#include <stdio.h>
int main()
{
    FLAG:;
    printf("Line: %d\n", __LINE__);
    printf("Line: %d\n", __LINE__);
    printf("Line: %d\n", __LINE__);
    goto FLAG;
    return 0;
}

```

程序明显地陷入了一个死循环，在 11 12 13 中不断循环。

## 结合 if 语句，实现循环语句

对循环语句进行分析，可以很明确的发现，循环语句就是这些功能

![1672740268300](image/C语言中的循环语句/1672740268300.png)

由此，我们可以结合 if 语句 与 goto 语句，实现一个真正意义上的循环语句。

![1672740512739](image/C语言中的循环语句/1672740512739.png)

```c
/*
    goto_third.c
    Use the goto to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int condition;
    condition = 1 > 0;
    LOOP:;
    {
        printf("This is line %d\n", __LINE__);
    }
    if (condition = 1 > 0)
        goto LOOP;  
    return 0;
}
```

对判断条件与输入数据进行改造，就可以实现一个更具实际意义的循环。

比如，我们可以让用户输入一个数字 n，计算从 1 到 n 的数字和

![1672740821456](image/C语言中的循环语句/1672740821456.png)

```c
/*
    goto_fourth.c
    Use the goto to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int condition;
    int number;
    int i;
    int sum = 0;
    printf("Please give me a number:");
    scanf("%d", &number);
    if (number <= 0)
    {
        printf("The input should >= 1.\n");
        return -1;
    }
    i = 0;
    condition = i <= number;
    LOOP:;
    {
        sum += i;
        i++;
    }
    if (condition = i <= number)
        goto LOOP;
    printf("The sum is %d\n", sum);  
    return 0;
}
```

可以发现，尽管程序顺利地实现了我们的目标，但是，它非常臃肿，难以理解，因此，我们需要使用更为简洁的循环语句。

## for 循环语句

C语言的循环语句殊途同归，下面，我们先用 for 循环语句改写这个程序。

![1672741244563](image/C语言中的循环语句/1672741244563.png)

```c
/*
    for.c
    Use the for statement to solve the problem.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    int sum = 0;
    printf("Please give me a number:");
    scanf("%d", &number);
    if (number <= 0)
    {
        printf("The input should >= 1.\n");
        return -1;
    }
    for (int i = 0;i <= number;i++)
    {
        sum += i;
    }
    printf("The sum is %d\n", sum);
    return 0;
}

```

很明显地发现，循环语句让我们的代码变得简洁，易于理解，下面我们来具体地分析一下 for 循环语句。

```c
for (init statement;condition;loop statement)
```

for 语句由三个部分组成，其中，每一个部分都是可选的，换而言之，我们当然可以什么都不写

```c
for(;;)
```

这就是一个标准的死循环

下面我们简单分析一下这三个部分

+ init statement
  初始化语句，在循环开始前**执行一次**
+ condition
  条件判断，根据逻辑运算的结果，决定是否执行循环语句
+ loop statement
  在循环语句块中的一切执行完成后，loop statement 将会执行一次
  （每一轮循环都会伴随 loop statement 的执行）

## while 循环语句 & do-while 循环语句

其它的循环语句更加简化了这个问题，while 循环可以被当作为一个简化版的 for 语句

```c
while (condition)
/* 等价于 */
for (;condition;)
```

我们可以继续将上面那个程序改写为 while 循环语句的形式

![1672742239275](image/C语言中的循环语句/1672742239275.png)

```c
/*
    while_achieve.c
    Use the while statement to solve the problem.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    int sum = 0;
    printf("Please give me a number:");
    scanf("%d", &number);
    if (number <= 0)
    {
        printf("The input should >= 1.\n");
        return -1;
    }
    int i = 0;
    while (i <= number)
    {
        sum += i;
        i++;
    }
    printf("The sum is %d\n", sum);
    return 0;
}

```

而采用 do-while 改写的形式如下

![1672742348425](image/C语言中的循环语句/1672742348425.png)

```c
/*
    do_while.c
    Use the do-while statement to solve the problem.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    int sum = 0;
    printf("Please give me a number:");
    scanf("%d", &number);
    if (number <= 0)
    {
        printf("The input should >= 1.\n");
        return -1;
    }
    int i = 0;
    do
    {
        sum += i;
        i++;
    }
    while (i <= number);
    printf("The sum is %d\n", sum);
    return 0;
}

```

可以发现，他们就是同一种模型的不同表达形式。

## break & continue 语句

为了提供更加灵活的循环控制功能，C语言也提供了有用的 break 与 continue 语句，用于帮助我们更好的进行循环处理。

+ break 语句
  break 语句用于直接跳出循环
+ continue 语句
  continue 语句结束当前的循环

学习一项计算机知识的最好方法，就是动手实践，因此我们将动手制作一个质数判断程序。

![1672744632008](image/C语言中的循环语句/1672744632008.png)

```c
/*
    prime.c
    Judge a number is or not a prime.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    int flag = 0;
    printf("Please input a number:");
    scanf("%d", &number);
    if (number <= 1)
    {
        printf("number should > 1\n");
        return -1;
    }
    for (int i = 1;i < number;i++)
    {
        if (0 == number % i)
        {
            if (1 == i)
            {
                continue;
            }
            else
            {
                flag = 1;
                break;
            }
        }
    }
    if (flag)
    {
        printf("It's not a prime\n");
    }
    else
    {
        printf("It's a prime\n");
    }
    return 0;
}
```

这个程序很好地体现了 break 与 continue 的功能，其中，break 会在“不是质数"判断生效以后，立即结束循环，而 continue 则是当数字为 1 时，中止本轮循环。

## 案例：猜数字游戏

结合循环语句、if 语句，我们可以制作出一个简单的猜数字游戏，它的规则如下：

+ 使用随机数生成函数，构造一个位于 0 - 100 之间的数字
+ 让用户不断输入，猜这个数字
+ 如果猜到了，游戏结束

在这之前，我们先引入 switch 语句的有关知识

switch 语句在对于固定常量的情况下，较之于 if 语句，具有更好的可读性与效率。

我们在此展现一个案例

![1672972184505](image/C语言中的逻辑语句与循环语句/1672972184505.png)

```c
/*
    switch.c
    Use the switch statement to solve the problem.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char input = '\0';
    puts("Welcome to see you, you can ask me a question:");
    puts("1. Which system I am using?");
    puts("2. Which compiler I am using?");
    puts("3. Which Language I want to study?");
    puts("q. Just quit.");
    input = getchar();
    switch (input)
    {
        case '1':
        {
            puts("Fedora Linux");
            break;
        }
        case '2':
        {
            puts("GCC");
            break;
        }
        case '3':
        {
            puts("C");
            break;
        }
        case 'q':
        {
            puts("Bye");
            break;
        }
        default:
        {
            puts("I don't know what's really you wang to ask.");
            break;
        }
    }
    return 0;
}

```

这一案例中，我们让用户输入一些字符进行问题答案的查询，并运用 case 子句与 default 子句进行指定情况与默认情况的处理。

在这一基础上，我们继续引入C语言中随机数函数的有关知识。

C语言使用 srand 与 rand 函数进行随机数的生成，其中，srand 函数用于提供随机种子，而rand就会根据随机种子生成随机数。

值得注意的是，当随机种子一致的时候，rand 总会生成一样的序列（对同样的公式而言，一样的数据，总是一样的结果），所以 rand 又被称为 “伪随机数"

学习他们最好的方法，就是动手实践，下面我们集中展现他们

![1672990596703](image/C语言中的逻辑语句与循环语句/1672990596703.png)

```c
/*
    rand.c
    Use the rand to generate number
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
int main()
{
    srand(256);
    printf("%d %d %d %d\n", rand(), rand(), rand(), rand());
    return 0;
}

```

可以看出，对于同样的随机种子256，rand函数生成的结果都是一模一样的。

> 最好的随机种子就是时间

所以，我们会使用C语言时间函数库对随机种子进行初始化。

由此，结合 switch 语句，我们写出了一个游戏。

![1672991191887](image/C语言中的逻辑语句与循环语句/1672991191887.png)

```c
/*
    number_game.c
    Achieve the number game
    BeginnerC
*/
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
int main()
{
    int target_number = 0;
    char input_choose = 0;
    int input_number = 0;
    srand(time(0));
    /* Generate a number 0 - 100 */
    target_number = rand() % 101;
    printf("Would you like to start:");
    input_choose = getchar();
    switch (input_choose)
    {
        case 'Y':
        case 'y':
        {
            /* Do nothing */
            ;
            break;
        }
        case 'N':
        case 'n':
        {
            exit(0);
            break;
        }
        default:
        {
            puts("I don't know what's you really want, quit.");
            exit(0);
            break;
        }
    }
    while (1)
    {
        printf("Input a number(0 - 100):");
        scanf("%d", &input_number);
        if (input_number < target_number)
        {
            puts("Small");
        }
        else if (input_number > target_number)
        {
            puts("Big");
        }
        else
        {
            break;
        }
    }
    printf("Equal\n");
    return 0;
}

```

在这个案例之中，我们使用一个 switch 来询问用户是否愿意开始（对于行为相同的选项，我们可以采取如此并列case 子句的方式)，并使用一个 while 循环让用户能够一直猜出数字来。

这便是一个简单的集中应用。
