# windows 下的C语言编译器配置

## 安装 gcc for windows

我们为你准备了[一个仓库](https://gitee.com/IamBeginnerC/gcc-compiler-for-windows)，参照上面的过程操作即可。

## 测试运行 C语言 代码

我们的所有代码都使用命令行方式编译运行，这就需要我们掌握 shell 的运行。

在 windows 系统中，启动 shell 的办法，就是按下快捷键（win + r）打开“运行”

之后输入 cmd 就可以启动 windows shell

![1673667569437](image/windows下的C语言编译器配置/1673667569437.png)

```shell
gcc --version
```

在这里，我们使用这一指令测试 gcc 的版本，接下来，我们开始测试第一个 C语言 程序

![1673667693240](image/windows下的C语言编译器配置/1673667693240.png)

启动 vscode，新建一个新的文件并保存（**后缀名标记为 .c** 代表C语言代码文件）

![1673667754237](image/windows下的C语言编译器配置/1673667754237.png)

```c
/*
    try.c
    The c file to try the gcc
    BeginnerC
*/
#include <stdio.h>
int main()
{
    printf("Hello World\n");
    return 0;
}
```

```shell
# use the gcc to compile
# format: gcc [source path] -o [output path]
# then run
```

案例示范为如下：

![1673667930178](image/windows下的C语言编译器配置/1673667930178.png)
