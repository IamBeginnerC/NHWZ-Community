/*
    function_pointer.c
    Use the function pointer to solve the problem
    BeginnerC
*/
#include <stdio.h>
int Add(int number_1, int number_2)
{
    return number_1 + number_2;
}
int main()
{
    int (*function)(int, int) = Add;
    printf("%u %u %d\n", function, Add, function(256, 256));
    return 0;
}
