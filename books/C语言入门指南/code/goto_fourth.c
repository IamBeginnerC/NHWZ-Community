/*
    goto_fourth.c
    Use the goto to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int condition;
    int number;
    int i;
    int sum = 0;
    printf("Please give me a number:");
    scanf("%d", &number);
    if (number <= 0)
    {
        printf("The input should >= 1.\n");
        return -1;
    }
    i = 0;
    condition = i <= number;
    LOOP:;
    {
        sum += i;
        i++;
    }
    if (condition = i <= number)
        goto LOOP;
    printf("The sum is %d\n", sum);    
    return 0;
}