/*
    char_array.c
    Use the char array to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char string[] = "Hello World";
    for (int i = 0;i < sizeof(string);i++)
    {
        printf("%d ", string[i]);
    }
    puts("");
    for (int i = 0;i < sizeof(string);i++)
    {
        printf("%c ", string[i]);
    }
    puts("");
    return 0;
}
