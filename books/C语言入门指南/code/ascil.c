/*
    ascil.c
    Print the ascil table
    BeginnerC
*/
#include <stdio.h>
int main()
{
    for (int i = 0; i < 256 ;i++)
    {
        printf("%d => %c \t", i, i);
        if (i % 9 == 0)
        {
            printf("\n");
        }
    }
    printf("\n");
    return 0;
}