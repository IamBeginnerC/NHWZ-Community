/*
    array_2.c
    Use the array to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int array[10] = {256, 255, 254};
    printf("%d %d\n", array[0], array[2]);
    return 0;
}
