/*
    calc_circle_area.c
    Calc the circle area
    BeginnerC
*/
#include <stdio.h>
int main()
{
    double pi = 3.14;
    float r = 2;
    printf("The area of circle is :%f\n", pi * r * r);
    return 0;
}