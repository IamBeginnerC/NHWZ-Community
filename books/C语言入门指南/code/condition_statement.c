/*
    condition_statement.c
    Show the usage of condition statement
    BeginnerC
*/
#include <stdio.h>
int main()
{
    1 > 0 ? puts("1 > 0"), puts("That's true") : puts("1 <= 0"), puts("That's false");
    return 0;
}
