/*
    function_second.c
    Use the function to solve the problem
    BeginnerC
*/
#include <stdio.h>
/*
    Pow
        Calc the a^b
    Argument
        a
            original number
        b
            power number
    Return Value
        The result is a^b
*/
double Pow(double a, double b)
{
    if (b <= 1)
    {
        return a;
    }
    return a * Pow(a, b - 1);
}
int main()
{
    printf("%lf\n", Pow(2, 3));
    return 0;
}
