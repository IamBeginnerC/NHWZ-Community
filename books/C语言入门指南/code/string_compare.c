/*
    string_compare.c
    Compare the string
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char string_1[1024] = {};
    char string_2[1024] = {};
    int flag = 0;
    scanf("%s", string_1);
    scanf("%s", string_2);
    for (int i = 0;i < 1024;i++)
    {
        if (string_1[i] != string_2[i])
        {
            flag = 1;
            break;
        }
    }
    if (flag)
    {
        printf("Not Equal\n");
    }
    else
    {
        printf("Equal\n");
    }
    return 0;
}
