/*
    do_while.c
    Use the do-while statement to solve the problem.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    int sum = 0;
    printf("Please give me a number:");
    scanf("%d", &number);
    if (number <= 0)
    {
        printf("The input should >= 1.\n");
        return -1;
    }
    int i = 0;
    do
    {
        sum += i;
        i++;
    }
    while (i <= number);
    printf("The sum is %d\n", sum);
    return 0;
}
