/*
    switch.c
    Use the switch statement to solve the problem.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char input = '\0';
    puts("Welcome to see you, you can ask me a question:");
    puts("1. Which system I am using?");
    puts("2. Which compiler I am using?");
    puts("3. Which Language I want to study?");
    puts("q. Just quit.");
    input = getchar();
    switch (input)
    {
        case '1':
        {
            puts("Fedora Linux");
            break;
        }
        case '2':
        {
            puts("GCC");
            break;
        }
        case '3':
        {
            puts("C");
            break;
        }
        case 'q':
        {
            puts("Bye");
            break;
        }
        default:
        {
            puts("I don't know what's really you wang to ask.");
            break;
        }
    }
    return 0;
}
