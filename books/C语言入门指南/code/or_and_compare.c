/*
    or_and_compare.c
    Show the difference of && and ||
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int result;
    result = (1 != 2) && (3 == 4);
    printf("%d\n", result);
    result = (1 != 2) || (3 == 4);
    printf("%d\n", result);
    return 0;
}