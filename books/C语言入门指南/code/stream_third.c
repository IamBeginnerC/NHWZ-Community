/*
    stream_third.c
    Use the stream to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char c = '\0';
    while (EOF != (c = getc(stdin)))
        putc(c, stdout);
    return 0;
}
