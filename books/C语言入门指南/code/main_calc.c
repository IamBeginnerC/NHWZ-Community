/*
    main_calc.c
    Use the main arguemnt to solve the problem
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
    double sum = 0;
    for (int i = 1;i < argc;i++)
    {
        sum += atof(argv[i]);
    }
    printf("Sum: %lf\n", sum);
    return 0;
}
