/*
    memory_copy.c
    Use the pointer to copy the data
    BeginnerC
*/
#include <stdio.h>
#include <time.h>
void GenerateNumberList(int array[], int count)
{
    for (int i = 0; i < count; i++)
    {
        array[i] = i;
    }
}
int main()
{
    int array[1000] = {};
    int array_backup[1000] = {};
    int count_memory = 0;
    unsigned begin, end;

    begin = clock();
    for (unsigned int i = 0; i < 1000000; i++)
    {
        count_memory = 0;
        for (double *p = (double *)(void *)array; count_memory < sizeof(array); p++)
        {
            *(double *)(void *)((char *)(void *)array_backup + count_memory) = *p;
            count_memory += sizeof(double);
        }
    }
    end = clock();
    printf("Time: %u ms\n", (end - begin) * 1000 / CLOCKS_PER_SEC);

    begin = clock();
    for (unsigned int i = 0; i < 1000000; i++)
    {
        count_memory = 0;
        for (char *p = (char *)(void *)array; count_memory < sizeof(array); p++)
        {
            *(char *)(void *)((char *)(void *)array_backup + count_memory) = *p;
            count_memory += sizeof(char);
        }
    }
    end = clock();
    printf("Time: %u ms\n", (end - begin) * 1000 / CLOCKS_PER_SEC);
    return 0;
}
