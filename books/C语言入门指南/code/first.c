/*
    first.c
    The first C language program
    BeginnerC
*/
#include <stdio.h>
int main()
{
    printf("Hello world!\n");
    return 0;
}
