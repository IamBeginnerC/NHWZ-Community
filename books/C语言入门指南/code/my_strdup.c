/*
    my_strdup.c
    Use the strdup function
    BeginnerC
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
    my_strdup
        Achieve the strdup
    Arguemnt
        string
            The string want to duplicate
    Return Value
        The new string
*/
char *my_strdup(const char *string)
{
    char *result = NULL;
    unsigned length = strlen(string);
    if (0 == length)
    {
        return NULL;
    }
    result = (char*)malloc(sizeof(char) * length + 1);
    if (NULL == result)
    {
        return NULL;
    }
    memcpy(result, string, length);
    return result;
}
int main()
{
    char string[] = "Hello world";
    char *string_backup = NULL;
    string_backup = my_strdup(string);
    puts(string_backup);
    free(string_backup);
    return 0;
}
