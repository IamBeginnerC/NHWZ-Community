/*
    memory_run_out.c
    Show the result of not use free
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
int main()
{
    void *ptr = NULL;
    for (int i = 0;i < 1024 * 1024 * 2;i++)
    {
        ptr = malloc(1024); /* 1KB */
        if (NULL == ptr)
            puts("Failed.");
    }
    getchar();
    return 0;
}
