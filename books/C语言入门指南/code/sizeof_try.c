/*
    sizeof_try.c
    Use the sizeof to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char c = 'A';
    printf("%d\n", sizeof(3.1415926));
    printf("%d\n", sizeof(3.14));
    printf("%d\n", sizeof('A'));
    printf("%d\n", sizeof('A' + 'B'));
    printf("%d\n", sizeof(c));
    printf("%d\n", sizeof(5 + 1));
    return 0;
}