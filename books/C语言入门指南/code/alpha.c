/*
    alpha.c
    Use toupper & tolower
    BeginnerC
*/
#include <stdio.h>
#include <ctype.h>
int main()
{
    char c;
    c = getchar();
    if (isalpha(c))
    {
        if (islower(c))
        {
            putchar(toupper(c));
        }
        else
        {
            putchar(tolower(c));
        }
    }
    else
    {
        printf("Not a alpha.\n");
    }
    return 0;
}