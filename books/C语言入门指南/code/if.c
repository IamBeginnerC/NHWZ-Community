/*
    if.c
    Use the if statement
    BeginnerC
*/
#include <stdio.h>
int main()
{
    if (1 > 0)
    {
        printf("1 > 0");
    }
    return 0;
}