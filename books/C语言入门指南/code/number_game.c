/*
    number_game.c
    Achieve the number game
    BeginnerC
*/
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
int main()
{
    int target_number = 0;
    char input_choose = 0;
    int input_number = 0;
    srand(time(0));
    /* Generate a number 0 - 100 */
    target_number = rand() % 101;
    printf("Would you like to start:");
    input_choose = getchar();
    switch (input_choose)
    {
        case 'Y':
        case 'y':
        {
            /* Do nothing */
            ;
            break;
        }
        case 'N':
        case 'n':
        {
            exit(0);
            break;
        }
        default:
        {
            puts("I don't know what's you really want, quit.");
            exit(0);
            break;
        }
    }
    while (1)
    {
        printf("Input a number(0 - 100):");
        scanf("%d", &input_number);
        if (input_number < target_number)
        {
            puts("Small");
        }
        else if (input_number > target_number)
        {
            puts("Big");
        }
        else
        {
            break;
        }
    }
    printf("Equal\n");
    return 0;
}
