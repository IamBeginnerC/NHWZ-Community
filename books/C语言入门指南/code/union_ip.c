/*
    union_ip.c
    Use the union to describle the IP address
    BeginnerC
*/
#include <stdio.h>
union IP
{
    struct address
    {
        int first : 8;
        int second : 8;
        int third : 8;
        int fourth : 8;
    }address;
    int ip_address;
};
int main()
{
    union IP IP = {127, 0, 0, 1};
    printf("%d.%d.%d.%d\n", IP.address.first, IP.address.second, IP.address.third, IP.address.fourth);
    printf("%x\n", IP.ip_address);
    return 0;
}