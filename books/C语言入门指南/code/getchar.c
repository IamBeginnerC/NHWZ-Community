/*
    getchar.c
    Use the getchar function
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char c;
    c = getchar();
    putchar(c);
    return 0;
}
