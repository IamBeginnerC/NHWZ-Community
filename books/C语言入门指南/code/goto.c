/*
    goto.c
    Use the goto to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    goto FLAG;
    printf("Line: %d\n", __LINE__);
    printf("Line: %d\n", __LINE__);
    FLAG:;
    printf("Line: %d\n", __LINE__);
    return 0;
}
