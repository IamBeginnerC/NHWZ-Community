/*
    float.c
    Test the float type
    BeginnerC
*/
#include <stdio.h>
int main()
{
    float number = 3.14159265358;
    double number_2 = 3.14159265358;
    printf("%.12lf\n", number);
    printf("%.12lf\n", number_2);
    return 0;
}