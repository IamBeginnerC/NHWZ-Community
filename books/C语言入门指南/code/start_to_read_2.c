/*
    start_to_read_2.c
    Start to read the user's input data
    （Second version）
    BeginnerC
*/
#include <stdio.h>
int main()
{
    double number, number_2;
    char c;
    scanf("%lf %c %lf", &number, &c, &number_2);
    printf("%lf %c %lf\n", number, c, number_2);
    return 0;
}