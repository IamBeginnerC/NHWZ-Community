/*
    prime.c
    Judge a number is or not a prime.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    int flag = 0;
    printf("Please input a number:");
    scanf("%d", &number);
    if (number <= 1)
    {
        printf("number should > 1\n");
        return -1;
    }
    for (int i = 1;i < number;i++)
    {
        if (0 == number % i)
        {
            if (1 == i)
            {
                continue;
            }
            else
            {
                flag = 1;
                break;
            }
        }
    }
    if (flag)
    {
        printf("It's not a prime\n");
    }
    else
    {
        printf("It's a prime\n");
    }
    return 0;
}