/*
    scanf_right.c
    Use the point to the scanf
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number;
    int *p = &number;
    scanf("%d", p);
    printf("%d %d\n", number, *p);
    return 0;
}