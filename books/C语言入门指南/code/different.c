/*
    different.c
    Show the difference of the unsigned & long
    BeginnerC
*/
#include <stdio.h>
int main()
{
    short number = 0b1111111111111111;
    unsigned short number_1 = 0xFFFF;
    printf("%d %d\n", sizeof(number), sizeof(number_1));
    printf("%d %d\n", number, number_1);
    return 0;
}