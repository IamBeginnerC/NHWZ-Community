/*
    point_third.c
    Rotation of points
    BeginnerC
*/
#include <stdio.h>
#include <math.h>
#define PI 3.1415926
struct point
{
    double x;
    double y;
};
/*
    AngleToRad
        Calc the Rad
    Arugument
        angel
            The angel want to calc
    Return Value
        The corresponding rad
*/
double AngleToRad(double angel)
{
    return angel * (PI / 180);
}
/*
    PointRoute
        Calc the route process
    Argument
        x
            The x of the point
        y
            The y of the point
        angle
            The angle
    Return Value
        The new point
*/
struct point PointRoute(double x, double y, double angle)
{
    struct point result = {};
    angle = AngleToRad(angle);
    result.x = x * cos(angle) - y * sin(angle);
    result.y = x * sin(angle) + y * cos(angle);
    return result;
}
int main()
{
    double x = 12, y = 12;
    struct point point_result = {0, 0};
    point_result = PointRoute(x, y, 36);
    printf("(%lf, %lf) to (%lf, %lf)\n", x, y, point_result.x, point_result.y);
    return 0;
}
