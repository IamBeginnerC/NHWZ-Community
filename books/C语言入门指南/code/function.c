/*
    function.c
    Use the function to solve the problem
    BeginnerC
*/
#include <stdio.h>
#define C 299792458
/*
    calc
        Calc the E = mc^2
    Argument
        m
            The mass
    Return Value
        The energy
*/
double calc(double m)
{
    return m * C * C;
}
int main()
{
    double m = 0.0;
    scanf("%lf", &m);
    printf("E = %f\n", calc(m));
    return 0;
}
