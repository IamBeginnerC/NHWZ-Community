/*
    compare.c
    Judge the number
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number, number_2;
    scanf("%d %d", &number, &number_2);
    if (number > number_2)
    {
        printf("%d > %d\n", number, number_2);
    }
    else
    {
        printf("%d < %d\n", number, number_2);
    }
    return 0;
}