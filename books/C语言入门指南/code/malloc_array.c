/*
    malloc_array.c
    Use the malloc to create a array
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
int* GenerateNumberList(int count)
{
    int *result = NULL;
    if (0 >= count)
    {
        return NULL;
    }
    result = (int*)malloc(count * sizeof(int));
    for (int i = 0;i < count;i++)
    {
        result[i] = i;
    }
    return result;
}
void PrintNumberList(int array[], int count)
{
    for (int i = 0;i < count;i++)
    {
        printf("%d\n", array[i]);
    }
}
int main()
{
    int *array = NULL;
    array = GenerateNumberList(10);
    if (NULL == array)
        return -1;
    PrintNumberList(array, 10);
    return 0;
}
