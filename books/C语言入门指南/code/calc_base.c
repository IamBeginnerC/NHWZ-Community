/*
    calc_base.c
    Show the usage of the calc
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int a = 1, b = 2;
    printf("%d + %d = %d\n", a, b, a + b);
    printf("%d - %d = %d\n", a, b, a - b);
    printf("%d * %d = %d\n", a, b, a * b);
    printf("%d / %d = %d\n", a, b, a / b);
    printf("%d / %d = %f\n", a, b, a / (double)b);
    return 0;
}
