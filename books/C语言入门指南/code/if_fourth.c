/*
    if_fourth.c
    Use the if statement to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    scanf("%d", &number);
    if (number >= 0)
    {
        if (number > 0)
        {
            printf("%d > 0\n", number);
        }
    }
    else
    {
        printf("%d < 0", number);
    }
    return 0;
}
