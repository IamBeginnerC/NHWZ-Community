/*
    string_array.c
    Print the string
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char string[] = "C Language";
    for (int i = 0;i < sizeof(string);i++)
    {
        putchar(string[i]);
    }
    return 0;
}