/*
    freopen.c
    Use the freopen function
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char c = '\0';
    freopen("output.txt", "w", stdout);
    while (EOF != (c = getchar()))
        putchar(c);
    return 0;
}
