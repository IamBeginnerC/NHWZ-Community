/*
    if_test.c
    Test the if statement
    BeginnerC
*/
#include <stdio.h>
int main()
{
    if (0)
    {
        printf("That's true!\n");
    }
    if (1)
    {
        printf("That's true!\n");
    }
    if ('t')
    {
        printf("That's true!\n");
    }
    if (666)
    {
        printf("That's true!\n");
    }
    if (3.1415926)
    {
        printf("That's true!\n");
    }
    return 0;
}