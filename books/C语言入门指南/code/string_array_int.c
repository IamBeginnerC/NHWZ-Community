/*
    string_array_int.c
    Print the string
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int string[] = {'C', ' ', 'L', 'a', 'n', 'g', 'u', 'a', 'g', 'e', '\0'};
    for (int i = 0;i < sizeof(string) / sizeof(string[0]);i++)
    {
        putchar(string[i]);
    }
    return 0;
}