/*
    string_compare_final.c
    Compare the string
    (Final Version)
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char string_1[1024] = {};
    char string_2[12] = {};
    int flag = 0;
    scanf("%s", string_1);
    scanf("%s", string_2);
    for (int i = 0;string_1[i] && string_2[i];i++)
    {
        if (string_1[i] != string_2[i])
        {
            flag = 1;
            break;
        }
    }
    if (flag)
    {
        printf("Not Equal\n");
    }
    else
    {
        printf("Equal\n");
    }
    return 0;
}