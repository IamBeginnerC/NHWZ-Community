/*
    ptr_plusplus.c
    Show the meaning of pointer plus plus
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    int *ptr = &number;
    double number_2 = 0;
    double *ptr_2 = &number_2;
    char c = 0;
    char *ptr_3 = &c;
    printf("%u %u %u\n", ptr, ptr++, sizeof(int));
    printf("%u %u %u\n", ptr_2, ptr_2++, sizeof(double));
    printf("%u %u %u\n", ptr_3, ptr_3++, sizeof(char));
    return 0;
}
