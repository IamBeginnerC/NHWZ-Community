/*
    strdup.c
    Use the strdup function
    BeginnerC
*/
#include <stdio.h>
#include <string.h>
int main()
{
    char string[] = "Hello world";
    char *string_backup = NULL;
    string_backup = strdup(string);
    puts(string_backup);
    return 0;
}
