/*
    fopen.c
    Use the fopen to open the file
    BeginnerC
*/
#include <stdio.h>
int main()
{
    FILE *fp = NULL;
    int number_1, number_2, number_result;
    fp = fopen("new.txt", "w+");
    if (NULL == fp)
    {
        return -1;
    }
    fprintf(fp, "%d + %d = %d\n", 1, 2, 1 + 2);
    fflush(fp);
    fseek(fp, SEEK_SET, 0);
    fscanf(fp, "%d + %d = %d", &number_1, &number_2, &number_result);
    printf("%d + %d = %d\n", number_1, number_2, number_result);
    fclose(fp);
    return 0;
}
