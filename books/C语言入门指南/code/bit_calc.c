/*
    bit_calc.c
    Show the usage of bit calc
    BeginnerC
*/
#include <stdio.h>
#include <string.h>
/*
    PrintNumber
        Print the number in binary form
    Argument
        number
            The number want to print
    Return Value
        No value will return
*/
void PrintNumber(int number)
{
    char buffer[1024] = {};
    char bit[2] = {};
    if (0 == number)
    {
        printf("0");
    }
    while (number)
    {
        sprintf(bit, "%d", number % 2);
        strcat(buffer, bit);
        number /= 2;
    }
    for (int i = strlen(buffer) - 1;i >= 0;i--)
    {
        printf("%c", buffer[i]);
    }
    puts("");
}
int main()
{
    /* Show the usage of >> & <<  */
    PrintNumber(8);
    PrintNumber(8 >> 1);
    PrintNumber(8 << 1);
    /* Show the usage of & and ^ and | */
    PrintNumber(4);
    PrintNumber(4 & 8);
    PrintNumber(4 ^ 8);
    PrintNumber(4 | 8);
    return 0;
}
