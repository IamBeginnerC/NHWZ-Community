/*
    array_memory.c
    Show the array's essence
    BeginnerC
*/
#include <stdio.h>
void PrintNumberList(int *array, int count)
{
    for (int i = 0;i < count;i++)
    {
        printf("%d ", *(array + i));
    }
    puts("");
}
void GenerateNumberList(int array[], int count)
{
    for (int i = 0;i < count;i++)
    {
        array[i] = i;
    }
}
int main()
{
    int number_list[10] = {};
    int number_list_backup[10] = {};
    double *p = NULL;
    int count_memory = 0;
    GenerateNumberList(number_list, 10);
    for (p = (double*)(void*)number_list;count_memory < sizeof(number_list);p++)
    {
        *(double*)(void*)((char*)(void*)number_list_backup + count_memory) = *p;
        count_memory += sizeof(double);
    }
    PrintNumberList(number_list_backup, 10);
    return 0;
}
