/*
    while.c
    Use the while statement
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int i = 0;
    while (i < 10)
    {
        printf("%d ", i);
        i++;
    }
    return 0;
}
