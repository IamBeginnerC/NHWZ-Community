/*
    struct_bit.c
    Use the struct to descible the bit
    BeginnerC
*/
#include <stdio.h>
struct char_bit
{
    unsigned bit_1 : 1;
    unsigned bit_2 : 1;
    unsigned bit_3 : 1;
    unsigned bit_4 : 1;
    unsigned bit_5 : 1;
    unsigned bit_6 : 1;
    unsigned bit_7 : 1;
    unsigned bit_8 : 1;
};
int main()
{
    struct char_bit bits = {};
    char c = 27;
    bits = *(struct char_bit*)(void*)(&c);
    printf("%d %d %d %d %d %d %d %d\n", bits.bit_1, bits.bit_2, bits.bit_3, bits.bit_4 \
    , bits.bit_5, bits.bit_6, bits.bit_7, bits.bit_8);
    return 0;
}
