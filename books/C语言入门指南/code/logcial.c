/*
    logical.c
    Show the usage of logical calc
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int result = 0;
    result = 2 >= 1;
    printf("%d\n", result);
    result = 0 > 1;
    printf("%d\n", result);
    return 0;
}