/*
    calc_second.c
    Achieve a simple calc
    (Final version)
    BeginnerC
*/
#include <stdio.h>
#include <stdbool.h>
int main()
{
    double number_1 = 0, number_2 = 0;
    char c = 0;
    while (1)
    {
        scanf("%lf %c %lf", &number_1, &c, &number_2);
        if ('+' == c)
        {
            double result = number_1 + number_2;
            printf("%lf + %lf = %lf\n", number_1, number_2, result);
        }
        else if ('-' == c)
        {
            double result = number_1 - number_2;
            printf("%lf - %lf = %lf\n", number_1, number_2, result);
        }
        else if ('*' == c)
        {
            double result = number_1 * number_2;
            printf("%lf * %lf = %lf\n", number_1, number_2, result);
        }
        else if ('/' == c)
        {
            double result = number_1 / number_2;
            printf("%lf / %lf = %lf\n", number_1, number_2, result);
        }
        else
        {
            printf("Unknow\n");
            return -1;
        }
    }
    return 0;
}