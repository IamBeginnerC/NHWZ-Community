/*
    sizeof.c
    Print the size of double & float
    BeginnerC
*/
#include <stdio.h>
int main()
{
    printf("%d %d\n", sizeof(double), sizeof(float));
    return 0;
}