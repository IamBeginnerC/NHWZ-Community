/*
    stream_second.c
    Use the stream to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char c = '\0';
    while (EOF != (c = getchar()))
        putchar(c);
    return 0;
}
