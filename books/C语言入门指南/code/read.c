/*
    read.c
    Read the data to the value
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    double number_2 = 0;
    char c = '\0';
    scanf("%d %lf %c", &number, &number_2, &c);
    printf("%d %f %c\n", number, number_2, c);
    return 0;
}
