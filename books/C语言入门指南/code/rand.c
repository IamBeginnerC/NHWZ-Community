/*
    rand.c
    Use the rand to generate number
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
int main()
{
    srand(256);
    printf("%d %d %d %d\n", rand(), rand(), rand(), rand());
    return 0;
}
