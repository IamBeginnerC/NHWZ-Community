/*
    for.c
    Use the for statement to solve the problem.
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    int sum = 0;
    printf("Please give me a number:");
    scanf("%d", &number);
    if (number <= 0)
    {
        printf("The input should >= 1.\n");
        return -1;
    }
    for (int i = 0;i <= number;i++)
    {
        sum += i;
    }
    printf("The sum is %d\n", sum);
    return 0;
}
