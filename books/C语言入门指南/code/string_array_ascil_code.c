/*
    string_array_ascil_code.c
    Print the array ascil code
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char string[] = "C Language";
    for (int i = 0;i < sizeof(string);i++)
    {
        printf("%d => %c\n", string[i], string[i]);
    }
    return 0;
}