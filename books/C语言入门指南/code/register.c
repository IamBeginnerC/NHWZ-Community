/*
    register.c
    Use the register value
    BeginnerC
*/
#include <stdio.h>
#include <time.h>
int main()
{
    int start = 0, end = 0;
    register int count_1 = 0;
    int count_2 = 0;
    start = clock();
    for (int i = 0;i < 1000000;i++)
        count_1++;
    end = clock();
    printf("%f s\n", (double)(end - start) / CLOCKS_PER_SEC);
    start = clock();
    for (int i = 0;i < 1000000;i++)
        count_2++;
    end = clock();
    printf("%f s\n",(double)(end - start) / CLOCKS_PER_SEC);
    return 0;
}
