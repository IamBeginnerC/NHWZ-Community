/*
    print.c
    Print some character
    BeginnerC
*/
int main()
{
    printf("%c", '\a');
    printf("%c", '\?');
    printf("%c", '\n');
    return 0;
}
