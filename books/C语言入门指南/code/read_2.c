/*
    read_2.c
    Read the data to the value
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int ip_first = 0;
    int ip_second = 0;
    int ip_third = 0;
    int ip_fourth = 0;
    scanf("%d.%d.%d.%d", &ip_first, &ip_second, &ip_third, &ip_fourth);
    printf("%d %d %d %d\n", ip_first, ip_second, ip_third, ip_fourth);
    return 0;
}
