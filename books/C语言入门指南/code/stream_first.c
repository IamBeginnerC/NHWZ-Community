/*
    stream_first.c
    Use the stream to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char c = 'c';
    char read_char = 'n';
    ungetc(read_char, stdin);
    c = getchar();
    putchar(c);
    return 0;
}
