/*
    if_second.c
    Show the usage of if statement
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int number = 0;
    scanf("%d", &number);
    if (number % 2 == 0)
    {
        printf("%d is not a odd number\n", number);
    }
    else
    {
        printf("%d is a odd number\n", number);
    }
    return 0;
}