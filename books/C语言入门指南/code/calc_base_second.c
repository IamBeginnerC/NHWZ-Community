/*
    calc_base_second.c
    Show the usage of the calc
    BeginnerC
*/
#include <stdio.h>
int main()
{
    printf("%d\n", 3 / 5);
    printf("%f\n", 3 / 5);
    printf("%f\n", 3 / (double)5);
    printf("%f\n", 3 / 5.0);
    return 0;
}
