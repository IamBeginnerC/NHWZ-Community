/*
    char_int.c
    Show the relation of the char & int
    BeginnerC
*/
#include <stdio.h>
int main()
{
    printf("%d = %c\n", 'A', 'A');
    return 0;
}
