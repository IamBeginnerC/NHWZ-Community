/*
    mod.c
    Show the usage of mod
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int a = 7, b = 3;
    printf("%d\n", a % b);
    return 0;
}