/*
    void_ptr.c
    Show a case of void*
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int var = 0;
    void *ptr = &var;
    printf("%u %u\n", ptr, ptr++);
    return 0;
}
