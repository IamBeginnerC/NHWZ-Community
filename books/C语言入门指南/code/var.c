/*
    var.c
    Show the usage of var
    BeginnerC
*/
#include <stdio.h>
int main()
{
    // Define a var named number, int type
    int number = 100;
    // Define a var named PI, double type
    double PI = 3.14;
    // Define a var named letter, char type
    char letter = 'A';
    printf("%d %lf %c\n", number, PI, letter);
    return 0;
}