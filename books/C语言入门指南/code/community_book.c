/*
    community_book.c
    Describle some community books
    BeginnerC
*/
#include <stdio.h>
struct book
{
    char tag[256];
    char title[256];
    char content[256];
};
int main()
{
    struct book book_list[3] = {
        {"C语言", "C程序设计语言", "Hello World"}, 
        {"C语言", "C语言入门很简单", "马磊老师是我的编程启蒙人"}, 
        {"伦理学著作", "道德情操论", "人最大的缺点是自欺欺人"}
        };
    for (int i = 0;i < sizeof(book_list) / sizeof(book_list[0]);i++)
    {
        printf("[%s]\n\t%s\n\t\t%s\n", book_list[i].tag, book_list[i].title, book_list[i].content);
    }
    return 0;
}
