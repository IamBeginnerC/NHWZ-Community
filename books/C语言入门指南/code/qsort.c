/*
    qsort.c
    Use the qsort to solve the problem
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
int cmp(const void *element_1, const void *element_2)
{
    return *(int*)element_1 > *(int*)element_2;
}
int main()
{
    int array[] = {8, 9, 6, 6, 3, 2, 7, 5};
    qsort(array, sizeof(array) / sizeof(array[0]), sizeof(int), cmp);
    for (int i = 0;i < sizeof(array) / sizeof(array[0]);i++)
    {
        printf("%d ", array[i]);
    }
    puts("");
    return 0;
}
