/*
    point_second.c
    Rotation of points
    BeginnerC
*/
#include <stdio.h>
#include <math.h>
#define PI 3.1415926
struct point
{
    double x;
    double y;
};
/*
    AngleToRad
        Calc the Rad
    Arugument
        angel
            The angel want to calc
    Return Value
        The corresponding rad
*/
double AngleToRad(double angel)
{
    return angel * (PI / 180);
}
int main()
{
    struct point point = {12, 12}, point_result = {0, 0};
    double angle = 36;
    point_result.x = point.x * cos(AngleToRad(angle)) - point.y * sin(AngleToRad(angle));
    point_result.y = point.x * sin(AngleToRad(angle)) + point.y * cos(AngleToRad(angle));
    printf("(%lf, %lf) to (%lf, %lf)\n", point.x, point.y, point_result.x, point_result.y);
    return 0;
}
