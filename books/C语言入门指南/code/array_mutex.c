/*
    array_mutex.c
    Use the array to solve the problem
    BeginnerC
*/
#include <stdio.h>
int main()
{
    int mutex[2][2] = {{2, 2}, {2, 2}};
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0;j < 2;j++)
        {
            printf("%d ", mutex[i][j]);
        }
        puts("");
    }
    return 0;
}
