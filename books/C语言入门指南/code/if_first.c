/*
    if_first.c
    Use the if statement to solve the problem
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
int main()
{
    int number = 0;
    printf("Please input a number >= 0\n");
    scanf("%d", &number);
    if (number < 0)
    {
        printf("The number < 0, Quit.\n");
        exit(0);
    }
    printf("You input %d\n", number);
    return 0;
}
