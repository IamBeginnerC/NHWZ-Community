/*
    read_4.c
    Show the usage of getchar/putchar
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char c = '\0';
    c = getchar();
    putchar(c);
    return 0;
}