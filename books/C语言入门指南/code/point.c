/*
    point.c
    Rotation of points
    BeginnerC
*/
#include <stdio.h>
#include <math.h>
#define PI 3.1415926
/*
    AngleToRad
        Calc the Rad
    Arugument
        angel
            The angel want to calc
    Return Value
        The corresponding rad
*/
double AngleToRad(double angel)
{
    return angel * (PI / 180);
}
int main()
{
    double x = 12, y = 12;
    double angle = 36;
    double x_result = 0, y_result = 0;
    x_result = x * cos(AngleToRad(angle)) - y * sin(AngleToRad(angle));
    y_result = x * sin(AngleToRad(angle)) + y * cos(AngleToRad(angle));
    printf("(%lf, %lf) to (%lf, %lf)\n", x, y, x_result, y_result);
    return 0;
}