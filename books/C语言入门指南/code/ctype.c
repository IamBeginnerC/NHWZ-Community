/*
    ctype.c
    Judge the type of char
    BeginnerC
*/
#include <ctype.h>
#include <stdio.h>
int main()
{
    char c;
    c = getchar();
    if (isalpha(c))
    {
        printf("%c is a letter\n", c);
        if (isupper(c))
        {
            printf("Upper Letter\n");
        }
        else if (islower(c))
        {
            printf("Lower Letter\n");
        }
    }
    else if (isdigit(c))
    {
        printf("%c is a number\n", c);
    }
    else
    {
        printf("Unknow\n");
    }
    return 0;
}