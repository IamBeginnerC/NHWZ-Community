/*
    calc_first.c
    Achieve a simple calc
    BeginnerC
*/
#include <stdio.h>
#include <stdbool.h>
int main()
{
    double number_1 = 0, number_2 = 0;
    char c = 0;
    scanf("%lf%c%lf", &number_1, &c, &number_2);
    if ('+' == c)
    {
        printf("%lf\n", number_1 + number_2);
    }
    else if ('-' == c)
    {
        printf("%lf\n", number_1 - number_2);
    }
    else
    {
        printf("Unknow\n");
        return -1;
    }
    return 0;
}