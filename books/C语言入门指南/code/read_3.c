/*
    read_3.c
    Use the scanf to read the string
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char string[256] = {};
    scanf("%s", string);
    printf(string);
    return 0;
}
