/*
    string_length.c
    Calc the length of string
    BeginnerC
*/
#include <stdio.h>
int main()
{
    char string[1024] = {};
    int length = 0;
    scanf("%s", string);
    for (length = 0;string[length];length++)
        ;
    printf("The length is %d\n", length);
    return 0;
}