/*
    file_model_second.c
    Show the usage of the file model
    BeginnerC
*/
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
int main()
{
    int fd = 0;
    char read_buffer[32] = {""};
    fd = open("read.txt", O_RDONLY);
    if (fd < 0)
    {
        return -1;
    }
    read(fd, read_buffer, sizeof(read_buffer));
    printf(read_buffer);
    close(fd);
    return 0;
}