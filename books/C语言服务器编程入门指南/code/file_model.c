/*
    file_model.c
    Show the usage of the file model
    BeginnerC
*/
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
int main()
{
    int fd = 0;
    char write_buffer[] = {"Hello world\n"};
    fd = open("new.txt", O_WRONLY | O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if (fd < 0)
    {
        return -1;
    }
    write(fd, write_buffer, sizeof(write_buffer));
    close(fd);
    return 0;
}