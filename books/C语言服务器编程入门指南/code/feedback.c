/*
    feedback.c
    Show the data send from client
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#define BUFFER_SIZE 2048
int main(int argc, char *argv[])
{
    int server_fd = 0;
    char buffer[BUFFER_SIZE] = {};
    struct sockaddr_in address = {};
    if (argc != 2)
    { 
        puts("Usage: FeedBackServer <port>");
        return 0;
    }
    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd < 0)
    {
        puts("Error: socket failed");
        return -1;
    }
    address.sin_family = AF_INET;
    address.sin_port = htons(atoi(argv[1]));
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(server_fd, (const struct sockaddr *)&address, sizeof(address));
    listen(server_fd, 1);
    while (1)
    {
        int connect_fd = 0;
        connect_fd = accept(server_fd, NULL, 0);
        while (1)
        {
            memset(buffer, 0, BUFFER_SIZE);
            read(connect_fd, buffer, BUFFER_SIZE);
            printf(buffer);
        }
        close(connect_fd);
    }
    return 0;
}