/*
    feedback_second.c
    Show the data send from client & Response
    BeginnerC
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#define BUFFER_SIZE 2048
int main(int argc, char *argv[])
{
    int server_fd = 0;
    char buffer[BUFFER_SIZE] = {};
    struct sockaddr_in address = {};
    if (argc != 2)
    { 
        puts("Usage: FeedBackServer <port>");
        return 0;
    }
    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd < 0)
    {
        puts("Error: socket failed");
        return -1;
    }
    address.sin_family = AF_INET;
    address.sin_port = htons(atoi(argv[1]));
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(server_fd, (const struct sockaddr *)&address, sizeof(address));
    listen(server_fd, 1);
    while (1)
    {
        int connect_fd = 0;
        int ret = -1;
        char time_buffer[BUFFER_SIZE] = {};
        time_t timer;
        struct tm *Now;
        time( &timer );
        Now = localtime( &timer );
        connect_fd = accept(server_fd, NULL, 0);
        memset(buffer, 0, BUFFER_SIZE);
        while (ret < 0)
        {
            ret = read(connect_fd, buffer, BUFFER_SIZE);
        }
        sprintf(buffer, "HTTP/1.1 200 OK\r\n");
        sprintf(buffer, "%sContent-type: text/html\r\n", buffer);
        sprintf(buffer, "%sContent-length: %d\r\n", buffer, strlen("<html>Hello world</html>"));
        sprintf(buffer, "%s\r\n\r\n", buffer);
        write(connect_fd, buffer, strlen(buffer));
        write(connect_fd, "<html>Hello world</html>", strlen("<html>Hello world</html>"));
        sleep(4);
        close(connect_fd);
    }
    return 0;
}