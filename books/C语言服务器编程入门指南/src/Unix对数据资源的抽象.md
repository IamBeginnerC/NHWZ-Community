# Unix对数据资源的抽象

## 本地数据资源读写抽象模型

想要弄明白数据资源的抽象原理，我们就应当先从宏观上看待它的运作过程。

![1673833422365](image/Unix对数据资源的抽象/1673833422365.png)

如您所见，我们的定位，就是“应用程序"这个层次，关注的方面就是简单的数据读写，而更为复杂的硬件资源调配等事务，由操作系统负责。

在 Unix 中，所有需要关心的硬件资源，被抽象为“文件"，具体到代码中，就是调用数个固定的API函数。

![1673834824529](image/Unix对数据资源的抽象/1673834824529.png)

```c
/*
    file_model.c
    Show the usage of the file model
    BeginnerC
*/
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
int main()
{
    int fd = 0;
    char write_buffer[] = {"Hello world\n"};
    fd = open("new.txt", O_WRONLY | O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if (fd < 0)
    {
        return -1;
    }
    write(fd, write_buffer, sizeof(write_buffer));
    close(fd);
    return 0;
}
```

如您所见，我们使用了 Unix 提供的 open 函数，建立了一个新文件 new.txt，并指示 write 函数进行数据写入，最后使用 close 函数关闭文件（实质就是结束针对特定硬件区域的数据读写)。

借助系统 api 函数（操作系统的抽象），我们可以将工作专注于数据的读写上，而不需要过多关心复杂的硬件资源管理。

数据资源的读取，也与之类似。

![1673835342389](image/Unix对数据资源的抽象/1673835342389.png)

```c
/*
    file_model_second.c
    Show the usage of the file model
    BeginnerC
*/
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
int main()
{
    int fd = 0;
    char read_buffer[32] = {""};
    fd = open("read.txt", O_RDONLY);
    if (fd < 0)
    {
        return -1;
    }
    read(fd, read_buffer, sizeof(read_buffer));
    printf(read_buffer);
    close(fd);
    return 0;
}
```

如您所见，我们先使用 echo 与 重定向创建了一个名为 read.txt 的文本文件，并指示我们的程序用 open 获得这份数据的读取权限，最后调用 read 函数将其读取完成，并用 close 函数将其关闭。

我们将其模型简化为如下。

![1673837386261](image/Unix对数据资源的抽象/1673837386261.png)

在实际的资源读写中，我们往往需要更多的系统接口函数加以帮助（比如 lseek, fsync等)，这已经超出了我们这组文章的范围，故不继续深入探讨。

## 分布式数据资源读写抽象模型

![1673838273482](image/Unix对数据资源的抽象/1673838273482.png)

分布式数据资源读写，较之于本地的资源读写，情况更为复杂，这主要是因为：

1. 各个主机间的操作系统、硬件规格往往存在差异
2. 数据链路传输中，往往容易遇到延时、丢包（数据遗失）等问题
3. 其它的一些重要问题（比如网络安全)

针对这些情况，网络的设计者们，一样提出了许多重要的应对方案：

1. 按照网络中各个配件的功能与作用，划分层次（参见 网络分层模型）
2. 对于不同主机间的数据交换，制定对应的规范与应对措施，称为协议
3. 各个操作系统，针对这些协议、层次，设计了类似的 API 函数，从而保证了问题的简化

这就使得，面向软件开发者的抽象，再次变得简单。

Unix 将其依照一般性本地文件的抽象方法，将其抽象为“远程文件读写"。

![1673859036610](image/Unix对数据资源的抽象/1673859036610.png)

只不过，相对于一般的本地文件，这种远程文件的读写模型，要求我们考虑的内容与范围，也就更多，概括为：

1. 需要明确读写的网络地址
2. 需要明确主机间的通信方式（协议）
3. 需要对读写中可能出现的延时等问题加以考虑（参见稳健的 RIO 读写包）
4. 需要确定“客户-服务端"关系
5. 其它需要考虑的因素

在接下来，我们将正式动手制作第一个网络通信程序。
